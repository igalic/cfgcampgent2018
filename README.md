# Vox Pupuli - The Funny Community Journey

Fourth Edition?

In taking over Tim Meusel's talk, https://github.com/bastelfreak/cfgcampgent2018
since he can't be there, due to company related travel.

This is a short (re) Introduction into What (and who) Vox Pupuli is, and how it works.

## Recordings

You can find a write-up of this talk on my blog: https://blag.esotericsystems.at/articles/voxpupli-community-journey/
and you can find a video recording here: https://www.youtube.com/watch?v=qhqw88oU6kE

